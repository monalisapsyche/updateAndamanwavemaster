TO:     	Andaman Wave Master
Submission Information through andamanwavemaster.com
*************************************
PERSONAL DETAILS

Name:		(@Name@)		Title:  @title@
E-mail Address:  	@Email@
Address:		{
		@Address@
}
Country:    	@Country@
Fax No: 		@FaxNo@
Phone No:	@TelNo@

***TRAVEL INFORMATION***

Arrival Date:	@Service Date@
Departure Date:	@Until Date@

No of Person(s):	
Adult	@NOADL@	Child	@NOCHD@

Service Required:		@Service1@ Tour	   @Service2@ Transfer	@Service3@ Hotel
Hotel Name for pick-up: 	@Hotel Name@
Request Details:
{
@Request Details@
}

Additional Transfer if required:	@Apt Transfer@ Need Airport Transfer
				Arrival Flight No	@Arr Flt No@	Arrival Time    @Arr Time@	
				@Apt Transfer@ No Thanks
