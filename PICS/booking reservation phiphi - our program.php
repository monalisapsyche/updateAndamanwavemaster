<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BOOKING - RESERVATION ALL PROGRAM</title>

<?
	require_once("connecting_db.php");
	$con = connect_db("db_login");
	@mysql_query("SET NAMES UTF8");
?>

<script language="javascript">
	
	function calcTime(offset) {
		d = new Date();
		utc = d.getTime() + (d.getTimezoneOffset() * 60000);
		nd = new Date(utc + (3600000 * offset));
		var dd = nd.getUTCDate();
		var mm = nd.getUTCMonth() + 1;
		var yy = nd.getUTCFullYear();
		if(dd < 10){
			dd = "0" + dd;
		}
		if(mm < 10){
			mm = "0" + mm;
		}
		var today = dd + "/" + mm + "/" + yy;
		
		form1.txt_chkIn.value = today;
		form1.txt_chkOut.value = today;
	}
	
	function calcTimeTomorrow(offset) {
		d = new Date();
		utc = d.getTime() + (d.getTimezoneOffset() * 60000);
		nd = new Date(utc + (3600000 * offset) + 86400000);
		var dd = nd.getDate();
		var mm = nd.getMonth() + 1;
		var yy = nd.getFullYear();
		if(dd < 10){
			dd = "0" + dd;
		}
		if(mm < 10){
			mm = "0" + mm;
		}
		var tomorrow = dd + "/" + mm + "/" + yy;
		form1.txt_chkOut.value = tomorrow;
	}
	
	function init(){
		calcTime('+7')
	}

	window.onload=init;
</script>

<script language="javascript">

	function fncDayDiff(day2,day1) {
       	d2 = new Date( day2.substr(6,4), (parseInt(day2.substr(3,2),10) - 1), day2.substr(0,2), 0,0,0,0);
	   	d1 = new Date( day1.substr(6,4), (parseInt(day1.substr(3,2),10) - 1), day1.substr(0,2), 0,0,0,0);
       	result = d2 - d1; 
	}
	
	function fncSubmitDaytrip(strPage){ 
		document.form1.txt_chkIn.disabled = false;
		document.form1.txt_chkOut.disabled = false;
		document.form1.action = "http://www.andamanwavemaster.com/booking reservation phiphi - package daytrip.php"
		//document.form1.action = "http://localhost/Copy of web_andamanwavemaster/booking reservation phiphi - package daytrip.php"
		document.form1.target = "_blank";    // Open in a new window
		document.form1.submit();             // Submit the page
		document.form1.txt_chkIn.disabled = true;
		document.form1.txt_chkOut.disabled = true;
	}
	
	function fncSubmitOvernight(strPage){ 
		document.form1.txt_chkIn.disabled = false;
		document.form1.txt_chkOut.disabled = false;
		document.form1.action = "http://www.andamanwavemaster.com/booking reservation phiphi - package overnight.php"
		//document.form1.action = "http://localhost/Copy of web_andamanwavemaster/booking reservation phiphi - package overnight.php"
		document.form1.target = "_blank";    // Open in a new window
		document.form1.submit();             // Submit the page
		document.form1.txt_chkIn.disabled = true;
		document.form1.txt_chkOut.disabled = true;
	}
	
	function fncSubmitHotel(strPage){ 
		document.form1.txt_chkIn.disabled = false;
		document.form1.txt_chkOut.disabled = false;
		document.form1.action = "http://www.andamanwavemaster.com/booking reservation phiphi - hotel service.php"
		//document.form1.action = "http://localhost/Copy of web_andamanwavemaster/booking reservation phiphi - hotel service.php"
		document.form1.target = "_blank";    // Open in a new window
		document.form1.submit();             // Submit the page
		document.form1.txt_chkIn.disabled = true;
		document.form1.txt_chkOut.disabled = true;
	}
	
	function fncSubmitBoat(strPage){ 
		document.form1.txt_chkIn.disabled = false;
		document.form1.txt_chkOut.disabled = false;
		document.form1.action = "http://www.andamanwavemaster.com/booking reservation phiphi - boat transfer.php"
		//document.form1.action = "http://localhost/Copy of web_andamanwavemaster/booking reservation phiphi - boat transfer.php"
		document.form1.target = "_blank";    // Open in a new window
		document.form1.submit();             // Submit the page
		document.form1.txt_chkIn.disabled = true;
		document.form1.txt_chkOut.disabled = true;
	}
	
	function fncSubmitCar(strPage){ 
		document.form1.txt_chkIn.disabled = false;
		document.form1.txt_chkOut.disabled = false;
		document.form1.action = "http://www.andamanwavemaster.com/booking reservation phiphi - car transfer.php"
		//document.form1.action = "http://localhost/Copy of web_andamanwavemaster/booking reservation phiphi - car transfer.php"
		document.form1.target = "_blank";    // Open in a new window
		document.form1.submit();             // Submit the page
		document.form1.txt_chkIn.disabled = true;
		document.form1.txt_chkOut.disabled = true;
	}
	
	function fncShowProvince(SelectValue){	
		form1.sel_areaTour.length=0;
		form1.sel_province.length=0;
		form1.sel_daytrip.length=0;
		form1.sel_overnight.length=0;
		form1.sel_hotel.length=0;
		form1.sel_car.length=0;
		
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_province WHERE PROVINCE_ENG IN ('PKT','KBI')";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strProvinceEng = "<?=$objResult["PROVINCE_ENG"];?>";
			strProvinceNameEng = "<?=$objResult["PROVINCE_NAME(ENG)"];?>";
			
			mySubList[x,1] = strProvinceEng;
			mySubList[x,2] = strProvinceNameEng;
	
			var myOption = new Option(mySubList[x,2], mySubList[x,1])  
			form1.sel_province.options[form1.sel_province.length]= myOption	
	<?	}	?>	
	
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_area_tour ORDER BY AreaTour_Fullname ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strProvince = "<?=$objResult["Province"];?>";
			strAreaTourShortName = "<?=$objResult["AreaTour_Shortname"];?>";
			strAreaTourFullName = "<?=$objResult["AreaTour_Fullname"];?>";
			
			mySubList[x,1] = strProvince;
			mySubList[x,2] = strAreaTourShortName;
			mySubList[x,3] = strAreaTourFullName;
			
			if(mySubList[x,1] == form1.sel_province.value){
				var myOption = new Option(mySubList[x,3], mySubList[x,2])  
				form1.sel_areaTour.options[form1.sel_areaTour.length]= myOption		
			}
	<?	}	?>
		
		if(form1.sel_areaTour.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_areaTour.options[form1.sel_areaTour.length]= myOption	
		}
		
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_daytrip ORDER BY Daytrip_Name ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Daytrip_ID"];?>";
			strItem = "<?=$objResult["Daytrip_Name"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == form1.sel_areaTour.value){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_daytrip.options[form1.sel_daytrip.length]= myOption	
			}
	<?	}	?>	
	
		if(form1.sel_daytrip.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_daytrip.options[form1.sel_daytrip.length]= myOption	
			form1.bt_daytrip.disabled = true;
		}else{
			form1.bt_daytrip.disabled = false;
		}
		
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_overnight,tb_package WHERE tb_overnight.Package=tb_package.Package_ID GROUP BY Package";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Package_ID"];?>";
			strItem = "<?=$objResult["Package_Name"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == form1.sel_areaTour.value){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_overnight.options[form1.sel_overnight.length]= myOption	
			}
	<?	}	?>	
	
		if(form1.sel_overnight.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_overnight.options[form1.sel_overnight.length]= myOption	
			form1.bt_overnight.disabled = true;
		}else{
			form1.bt_overnight.disabled = false;
		}
		
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_hotel ORDER BY 'Hotel_Fullname' ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Hotel_Shortname"];?>";
			strItem = "<?=$objResult["Hotel_Fullname"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == form1.sel_areaTour.value){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_hotel.options[form1.sel_hotel.length]= myOption	
			}
	<?	}	?>	
	
		if(form1.sel_hotel.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_hotel.options[form1.sel_hotel.length]= myOption	
			form1.bt_hotel.disabled = true;
		}else{
			form1.bt_hotel.disabled = false;
		}
		
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_pickup ORDER BY 'AreaHotel_Name' ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			stAreaID = "<?=$objResult["Area_ID"];?>";
			strAreaHotelName = "<?=$objResult["AreaHotel_Name"];?>";
			
			mySubList[x,0] = stAreaID;
			mySubList[x,1] = strAreaHotelName;
	
			if(mySubList[x,0] == form1.sel_province.value){
				var myOption = new Option(mySubList[x,1], mySubList[x,0])  
				form1.sel_car.options[form1.sel_car.length]= myOption	
			}
	<?	}	?>	
	
		if(form1.sel_car.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_car.options[form1.sel_car.length]= myOption	
			form1.bt_car.disabled = true;
		}else{
			form1.bt_car.disabled = false;
		}
	}

	function fncShowProvinceDetail(SelectValue){	
		form1.rdo_Province.checked=true;
		form1.sel_areaTour.length=0;
		form1.sel_daytrip.length=0;
		form1.sel_overnight.length=0;
		form1.sel_hotel.length=0;
		form1.sel_car.length = 0;
		
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_area_tour ORDER BY AreaTour_Fullname ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["Province"];?>";
			strValue = "<?=$objResult["AreaTour_Shortname"];?>";
			strItem = "<?=$objResult["AreaTour_Fullname"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == SelectValue){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_areaTour.options[form1.sel_areaTour.length]= myOption		
			}
	<?	}	?>	
	
		if(form1.sel_areaTour.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_areaTour.options[form1.sel_areaTour.length]= myOption	
		}
		
		ShowDaytrip_Pro(SelectValue);
		ShowOvernight_Pro(SelectValue);
		ShowHotel_Pro(SelectValue);
		ShowCar_Pro(SelectValue);
	}
	
	function fncShowLocate(SelectValue){	
		form1.sel_areaTour.length=0;
		form1.sel_province.length=0;
		form1.sel_daytrip.length=0;
		form1.sel_overnight.length=0;
		form1.sel_hotel.length=0;
		form1.sel_car.length=0;
		
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_area_tour ORDER BY AreaTour_Fullname ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["Province"];?>";
			strValue = "<?=$objResult["AreaTour_Shortname"];?>";
			strItem = "<?=$objResult["AreaTour_Fullname"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
			
			var myOption = new Option(mySubList[x,0], mySubList[x,2])  
			form1.sel_areaTour.options[form1.sel_areaTour.length]= myOption	
	<?	}	?>	
	
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_province ORDER BY 'PROVINCE_NAME(ENG)' ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup_1 = "<?=$objResult["PROVINCE_ENG"];?>";
			strValue = "<?=$objResult["PROVINCE_ENG"];?>";
			strItem = "<?=$objResult["PROVINCE_NAME(ENG)"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup_1;
			mySubList[x,2] = strValue;
			
			if(strGroup == strGroup_1){	
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_province.options[form1.sel_province.length]= myOption		
			}
	<?	}	?>	
	
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_daytrip ORDER BY Daytrip_Name ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Daytrip_ID"];?>";
			strItem = "<?=$objResult["Daytrip_Name"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == form1.sel_areaTour.value){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_daytrip.options[form1.sel_daytrip.length]= myOption	
			}
	<?	}	?>
	
		if(form1.sel_daytrip.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_daytrip.options[form1.sel_daytrip.length]= myOption	
			form1.bt_daytrip.disabled = true;
		}else{
			form1.bt_daytrip.disabled = false;
		}
			
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_overnight,tb_package WHERE tb_overnight.Package=tb_package.Package_ID GROUP BY Package ORDER BY 'Package_Name' ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Package_ID"];?>";
			strItem = "<?=$objResult["Package_Name"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == form1.sel_areaTour.value){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_overnight.options[form1.sel_overnight.length]= myOption	
			}
	<?	}	?>
	
		if(form1.sel_overnight.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_overnight.options[form1.sel_overnight.length]= myOption	
			form1.bt_overnight.disabled = true;
		}else{
			form1.bt_overnight.disabled = false;
		}
			
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_hotel ORDER BY 'Hotel_Fullname' ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Hotel_Shortname"];?>";
			strItem = "<?=$objResult["Hotel_Fullname"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == form1.sel_areaTour.value){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_hotel.options[form1.sel_hotel.length]= myOption	
			}
	<?	}	?>
	
		if(form1.sel_hotel.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_hotel.options[form1.sel_hotel.length]= myOption	
			form1.bt_hotel.disabled = true;
		}else{
			form1.bt_hotel.disabled = false;
		}	
		
		strItem = "--";
		mySubList[x,0] = strItem;
			
		var myOption = new Option(mySubList[x,0])  
		form1.sel_car.options[form1.sel_car.length]= myOption	
		form1.bt_car.disabled = true;
	}
	
	function fncLocate(SelectValue) {	
		//form1.rdo_Locate.checked = true;
		form1.sel_daytrip.length=0;
		//form1.sel_province.length=0;
		form1.sel_overnight.length=0;
		form1.sel_hotel.length=0;
		
		<?	$intRows = 0;
			$strSQL = "SELECT * FROM tb_area_tour ORDER By 'Province' ASC";
			$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
			$intRows = 0;
			while($objResult = mysql_fetch_array($objQuery))
			{
				$intRows++;	?>
				x = <?=$intRows;?>;
				mySubList = new Array();
				
				strGroup = "<?=$objResult["Province"];?>";
				strValue = "<?=$objResult["AreaTour_Shortname"];?>";
				strItem = "<?=$objResult["AreaTour_Fullname"];?>";
		<?	}	?>	
		
		<?	$intRows = 0;
			$strSQL = "SELECT * FROM tb_province ORDER BY 'PROVINCE_NAME(ENG)' ASC";
			$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
			$intRows = 0;
			while($objResult = mysql_fetch_array($objQuery))
			{
				$intRows++;	?>
				x = <?=$intRows;?>;
				mySubList = new Array();
				
				strGroup_1 = "<?=$objResult["PROVINCE_ENG"];?>";
				strValue = "<?=$objResult["PROVINCE_ENG"];?>";
				strItem = "<?=$objResult["PROVINCE_NAME(ENG)"];?>";
				
				mySubList[x,0] = strItem;
				mySubList[x,1] = strGroup_1;
				mySubList[x,2] = strValue;
				
				if(strGroup == strGroup_1){
					if(form1.rdo_Locate.checked == true){
						form1.sel_province.length=0;
						var myOption = new Option(mySubList[x,0], mySubList[x,2])  
						form1.sel_province.options[form1.sel_province.length]= myOption	
					}
				}
				
		<?	}	?>	
		
			ShowDaytrip(SelectValue);
			ShowOvernight_AreaTour(SelectValue);
			ShowHotel(SelectValue);
			ShowCar(SelectValue);
	}
	
	function ShowDaytrip(SelectValue){
	<?
		$intRows = 0;
		$strSQL = "SELECT * FROM tb_daytrip ORDER BY Daytrip_Name ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;
	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Daytrip_ID"];?>";
			strItem = "<?=$objResult["Daytrip_Name"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
			
			if(mySubList[x,1] == SelectValue){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_daytrip.options[form1.sel_daytrip.length]= myOption	
			}
	<?
		}
	?>	
		if(form1.sel_daytrip.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_daytrip.options[form1.sel_daytrip.length]= myOption	
			form1.bt_daytrip.disabled = true;
		}else{
			form1.bt_daytrip.disabled = false;
		}
	}
	
	function ShowOvernight_AreaTour(SelectValue){
	<?
		$intRows = 0;
		$strSQL = "SELECT * FROM tb_overnight,tb_package WHERE tb_overnight.Package=tb_package.Package_ID GROUP BY Package";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;
	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Package_ID"];?>";
			strItem = "<?=$objResult["Package_Name"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
		
			if(mySubList[x,1] == SelectValue){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_overnight.options[form1.sel_overnight.length]= myOption	
			}
	<?
		}
	?>	
		if(form1.sel_overnight.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_overnight.options[form1.sel_overnight.length]= myOption	
			form1.bt_overnight.disabled = true;
		}else{
			form1.bt_overnight.disabled = false;
		}
	}
	
	function ShowHotel(SelectValue){
	<?
		$intRows = 0;
		$strSQL = "SELECT * FROM tb_hotel ORDER BY 'Hotel_Fullname' ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;
	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Hotel_Shortname"];?>";
			strItem = "<?=$objResult["Hotel_Fullname"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == SelectValue){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_hotel.options[form1.sel_hotel.length]= myOption	
			}
	<?
		}
	?>	
		if(form1.sel_hotel.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_hotel.options[form1.sel_hotel.length]= myOption	
			form1.bt_hotel.disabled = true;
		}else{
			form1.bt_hotel.disabled = false;
		}
	}
	
	function ShowDaytrip_Pro(SelectValue){
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_daytrip ORDER BY Daytrip_Name ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Daytrip_ID"];?>";
			strItem = "<?=$objResult["Daytrip_Name"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == form1.sel_areaTour.value){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_daytrip.options[form1.sel_daytrip.length]= myOption	
			}
	<?	}	?>	
	
		if(form1.sel_daytrip.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_daytrip.options[form1.sel_daytrip.length]= myOption	
			form1.bt_daytrip.disabled = true;
		}else{
			form1.bt_daytrip.disabled = false;
		}		
	}
	
	function ShowOvernight_Pro(SelectValue){
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_overnight,tb_package WHERE tb_overnight.Package=tb_package.Package_ID GROUP BY Package";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Package_ID"];?>";
			strItem = "<?=$objResult["Package_Name"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
		
			if(mySubList[x,1] == form1.sel_areaTour.value){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_overnight.options[form1.sel_overnight.length]= myOption	
			}
	<?	}	?>	
	
		if(form1.sel_overnight.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_overnight.options[form1.sel_overnight.length]= myOption	
			form1.bt_overnight.disabled = true;
		}else{
			form1.bt_overnight.disabled = false;
		}
	}

	function ShowHotel_Pro(SelectValue){
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_hotel ORDER BY 'Hotel_Fullname' ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strGroup = "<?=$objResult["AreaTour"];?>";
			strValue = "<?=$objResult["Hotel_Shortname"];?>";
			strItem = "<?=$objResult["Hotel_Fullname"];?>";
			
			mySubList[x,0] = strItem;
			mySubList[x,1] = strGroup;
			mySubList[x,2] = strValue;
	
			if(mySubList[x,1] == form1.sel_areaTour.value){
				var myOption = new Option(mySubList[x,0], mySubList[x,2])  
				form1.sel_hotel.options[form1.sel_hotel.length]= myOption	
			}
	<?	}	?>	
	
		if(form1.sel_hotel.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_hotel.options[form1.sel_hotel.length]= myOption	
			form1.bt_hotel.disabled = true;
		}else{
			form1.bt_hotel.disabled = false;
		}
	}
	
	function ShowCar_Pro(SelectValue){
	<?	$intRows = 0;
		$strSQL = "SELECT * FROM tb_pickup ORDER BY AreaHotel_Name ASC";
		$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
		$intRows = 0;
		while($objResult = mysql_fetch_array($objQuery))
		{
			$intRows++;	?>
			x = <?=$intRows;?>;
			mySubList = new Array();
			
			strAreaID = "<?=$objResult["Area_ID"];?>";
			strAreaHotelName = "<?=$objResult["AreaHotel_Name"];?>";
			
			mySubList[x,0] = strAreaID;
			mySubList[x,1] = strAreaHotelName;

			if(mySubList[x,0] == SelectValue){
				var myOption = new Option(mySubList[x,1], mySubList[x,1])  
				form1.sel_car.options[form1.sel_car.length]= myOption	
			}
	<?	}	?>	
	
		if(form1.sel_car.value == ""){
			strItem = "--";
			mySubList[x,0] = strItem;
			
			var myOption = new Option(mySubList[x,0])  
			form1.sel_car.options[form1.sel_car.length]= myOption	
			form1.bt_car.disabled = true;
		}else{
			form1.bt_car.disabled = false;
		}
	}
	
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

	function fncCheckDateToday(){
		d = new Date();
		utc = d.getTime() + (d.getTimezoneOffset() * 60000);
		nd = new Date(utc + (3600000 * 7));
		var dd = nd.getUTCDate();
		var mm = nd.getUTCMonth() + 1;
		var yy = nd.getUTCFullYear();
		if(dd < 10){
			dd = "0" + dd;
		}
		if(mm < 10){
			mm = "0" + mm;
		}
		var today = dd + "/" + mm + "/" + yy;
		
		fncDayDiff(today,form1.txt_chkIn.value);
		if(result > 0){
			alert('Check In Date is Wrong!!');
			form1.txt_chkIn.value = today;
			return false;
		}else{
			fncDayDiff(form1.txt_chkIn.value,form1.txt_chkOut.value);
			if(result > 0){
				form1.txt_chkOut.value = form1.txt_chkIn.value;
			}
		}
	}
	
	function fncCheckDate(){
		fncDayDiff(form1.txt_chkIn.value,form1.txt_chkOut.value);
		if(result > 0){
			alert('Please Check Date Again !!');
			form1.txt_chkOut.value = form1.txt_chkIn.value;
			return false; 
		}
	}

</script>

</head>

<body onload="init();">
<form id="form1" name="form1" method="post" action="javascript:return fncSubmit();"  >
  <table width="75%" border="0" align="center" bgcolor="#99CCFF" >
  <tr>
    <td width="36%"><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" background="PICS/LONG_BCH+JC3(BG).jpg">
          <tr>
            <td><div align="center"><font size="2"><strong><font color="#003366"><br />
                        <font color="#0066FF">ANDAMAN WAVE MASTER'S  SERVICES</font></font></strong><br />
                        <br />
            </font></div></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
        <td><table width="100%" border="0">
          <tr>
            <td><table width="100%" border="0" background="PICS/LONG_BCH+JC3(BG).jpg">
              <tr>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td width="37%" ><div align="right"><font color="#FFFFFF"><strong><font size="2">Departure / Check In Date :</font></strong></font></div></td>
                <td width="63%" colspan="2"><font color="#003366">
                  <input name="txt_chkIn" type="text" id="txt_chkIn"  style="text-align:center"  size="10" disabled="disabled" onchange="fncCheckDateToday();" />   	
                  <label><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('bt_chkIn','','PICS/calendars.jpg',0)"><img src="PICS/calendars_2.jpg" alt="Departute / Check In Date" name="bt_chkIn" width="20" height="20" border="0" id="bt_chkIn" /></a>
                  	<script type="text/javascript">
            		Calendar.setup({
				  		inputField    : "txt_chkIn",
				  		button        : "bt_chkIn",
			  	  		ifFormat    : "%d/%m/%Y"
           		 	});
					</script>  
                  </label>
                </font></td>
              </tr>
              <tr>
                <td ><div align="right"><font color="#FFFFFF"><strong><font size="2">Arrival / Check Out Date :</font></strong></font></div></td>
                <td colspan="2"><font color="#003366">
                  <input name="txt_chkOut" type="text" id="txt_chkOut"  style="text-align:center" size="10" disabled="disabled" onchange="fncCheckDate();"/>
                  <label><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('bt_chkOut','','PICS/calendars.jpg',0)"><img src="PICS/calendars_2.jpg" alt="Arrival / Check Out Date" name="bt_chkOut" width="20" height="20" border="0" id="bt_chkOut" /></a>
                  <script type="text/javascript">
            	Calendar.setup({
				  inputField    : "txt_chkOut",
				  button        : "bt_chkOut",
			  	  ifFormat    : "%d/%m/%Y"
           		 });
              </script>
                  </label>
                </font></td>
              </tr>
              <tr>
                <td width="37%" ><div align="right"><font color="#003366"><strong><font size="2">Adult (s) : </font></strong></font></div></td>
                <td colspan="2" ><select name="sel_adult" id="sel_adult" style="width:50px;">
                    <? for($i=1;$i<11;$i++){
					if($i == $_POST["sel_adult"]){ ?>
                    <option value="<? echo $i ?>" selected="selected"><? echo $i ?></option>
                    ";
				
              
                  <?	}else{ ?>
                    <option value="<? echo $i ?>"><? echo $i ?></option>
                    ";
				
              
                  <?	}
				  } 
			  ?>
                </select></td>
              </tr>
              <tr>
                <td ><div align="right"><font color="#003366"><strong><font size="2">Child (s) : </font></strong></font></div>
                    <font color="#003366"><strong><font size="2">
                    <label></label>
                    </font></strong></font></td>
                <td colspan="2" ><font size="2">
                  <select name="sel_child" id="sel_child" style="width:50px;">
                    <? for($i=0;$i<11;$i++){
						if($i == $_POST["sel_child"]){ ?>
                    <option value="<? echo $i ?>" selected="selected"><? echo $i ?></option>
                    ";
				
                
                    <? 		}else{ ?>
                    <option value="<? echo $i ?>"><? echo $i ?></option>
                    ";
				
                
                    <? 		}
				   } 
				?>
                  </select>
                </font></td>
              </tr>
              <tr>
                <td colspan="3" >&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
          <table width="100%" border="0">
            <tr>
              <td><table width="100%" border="0" background="PICS/LONG_BCH+JC3(BG).jpg">
                <tr>
                  <td width="21%">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td width="63%">&nbsp;</td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#003366"><font size="2"> Destination Selected :</font></font></div></td>
                  <td width="16%"><font color="#FFFFFF">
                    <label>
                    <input name="rdo_selectType" type="radio" id="rdo_Locate" value="1" checked="checked" onmouseover="this.style.cursor = 'hand'" onclick="fncShowLocate(this.value)"/>
                    <font color="#0066FF" size="2"> City / Location</font></label>
                  </font></td>
                  <td><strong><font color="#FFFFFF"><font size="2">
                    <select name="sel_areaTour" id="sel_areaTour" style="width:200px;" onchange="fncLocate(this.value)"  >
                      <option value="">Please Select</option>
                      <?
							$strSQL = "SELECT * FROM tb_area_tour ORDER BY AreaTour_Shortname ASC";
							$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
							while($objResult = mysql_fetch_array($objQuery))
							{
						?>
                      <option value="<?=$objResult["AreaTour_Shortname"];?>">
                      <?=$objResult["AreaTour_Fullname"];?>
                      </option>
                      <?
				}
			?>
                    </select>
                  </font></font></strong></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><font color="#FFFFFF">
                    <label>
                    <input type="radio" name="rdo_selectType" id="rdo_Province" value="2" onmouseover="this.style.cursor = 'hand'" onclick="fncShowProvince(this.value)" />
                    <font color="#0066FF" size="2">Province </font></label>
                  </font></td>
                  <td><strong><font color="#FFFFFF"><font size="2">
                    <select name="sel_province" id="sel_province" style="width:200px;" onchange="fncShowProvinceDetail(this.value)" >
                      <option value="">--</option>
                    </select>
                  </font></font></strong></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table>
          <table width="100%" border="0">
            <tr>
              <td><table width="100%" border="0" background="PICS/LONG_BCH+JC3(BG).jpg">
                <tr>
                  <td colspan="4">&nbsp;</td>
                </tr>

                <tr>
                  <td width="21%"><div align="right"><font color="#003366"><font size="2">Daytrip Program :</font></font></div></td>
                  <td colspan="2"><select name="sel_daytrip" id="sel_daytrip" style="width:400px;">
                      <option value="">--</option>
                  </select></td>
                  <td width="14%"><input type="button" name="bt_daytrip" id="bt_daytrip" value="Check Avail" style="width:110px;" disabled="disabled" onclick="javascript:return fncSubmitDaytrip();" onmouseover="this.style.cursor='hand'" /></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#003366"><font size="2"> Overnight Package :</font></font></div></td>
                  <td colspan="2"><select name="sel_overnight" id="sel_overnight" style="width:200px;" >
                      <option value="">--</option>
                  </select></td>
                  <td><input type="button" name="bt_overnight" id="bt_overnight" disabled="disabled" style="width:110px;" value="Check Avail" onclick="javascript:return fncSubmitOvernight();" onmouseover="this.style.cursor='hand'" /></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#003366"><font size="2">Hotel Service :</font></font></div></td>
                  <td colspan="2"><select name="sel_hotel" id="sel_hotel" style="width:200px;" >
                      <option value="">--</option>
                  </select></td>
                  <td><input type="button" name="bt_hotel" id="bt_hotel" value="Check Avail" disabled="disabled" style="width:110px;" onclick="javascript:return fncSubmitHotel();" onmouseover="this.style.cursor='hand'" /></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#003366"><font size="2">Boat Transfer :</font></font></div></td>
                  <td colspan="2"><select name="sel_boat" id="sel_boat" style="width:450px;" >
                    <?	$strSQL = "SELECT * FROM tb_route GROUP BY Route_Name";
						$objQuery = mysql_query($strSQL) or die ("Error Query [".$strSQL."]");
						while($objResult = mysql_fetch_array($objQuery))
						{	?>
							<option value="<?=$objResult["Route_Name"];?>"><?=$objResult["Route_Name"];?></option>
					<?	}	?>
                  </select></td>
                  <td><input type="button" name="bt_boat" id="bt_boat" value="Check Avail" style="width:110px;" onclick="javascript:return fncSubmitBoat();" onmouseover="this.style.cursor='hand'" /></td>
                </tr>
                <tr>
                  <td><div align="right"><font color="#003366"><font size="2">Car Transfer ( <font color="#0066FF">Private</font> ) :</font></font></div></td>
                  <td colspan="2"><select name="sel_car" id="sel_car" style="width:200px;" >
                  <option value="">--</option> 
                  </select></td>
                  <td><div align="left">
                    <input type="button" name="bt_car" id="bt_car" value="Check Avail" disabled="disabled" style="width:110px;" onclick="javascript:return fncSubmitCar();" onmouseover="this.style.cursor='hand'" />
                  </div></td>
                </tr>
                <tr>
                  <td colspan="4">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
          </table></td>
    </tr>
  </table>
  <div align="center"></div>
</form>
</body>
</html>